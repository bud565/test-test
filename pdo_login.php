<?php
session_start();
$host = "localhost";
$username = "root";
$password = "";
$database = "skripsi";
$message = "";
if(isset($_SESSION["nim"]))  
{  
    header("location:index.php");  
}
try
{
    $connect = new PDO("mysql:host=$host; dbname=$database", $username, $password);
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if(isset($_POST["login"]))
    {
        if(empty($_POST["nim"]) || empty($_POST["password"]))
        {
            $message = '<label>All fields are required</label>';
        }
        else
        {
            $query = "SELECT * FROM mahasiswa WHERE nim = :nim AND password = :password";
            $statement = $connect->prepare($query);
            $statement->execute(
                array(
                    'nim'     =>     $_POST["nim"],
                    'password'     =>     md5($_POST["password"])
                )
            );
            $count = $statement->rowCount();
            if($count > 0)
            {
				$_SESSION["nim"] = $_POST{"nim"};
				while($result = $statement->fetch()){
					
					$_SESSION["author"] = $result["nama"];
				}
                header("location:index.php");
            }
            else
            {
                $message = '<label>Wrong Data</label>';
            }
        }
    }
    if(isset($_POST["register"])) 
    {
        header("location:pdo_register.php");
    }
}
catch(PDOException $error)
{
    $message = $error->getMessage();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
		<link rel="shortcut icon" href="flat.ico">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<script src="bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="index.php">HOME</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="pdo_show.php">LIST</a>
				</li>
			</ul>
		</nav>
	
		<br />
        <div class="container" style="width:500px;">
			<?php
			if(isset($message))
            {
                echo '<label class="text-danger">'.$message.'</label>';
            }
            ?>
            <h3 align="center">Login</h3><br />
            <form method="post">
                <label>NIM</label>
                <input type="text" name="nim" class="form-control" />
                <br />
                <label>Password</label>
                <input type="password" name="password" class="form-control" />
                <br />
                <input type="submit" name="login" class="btn btn-info" value="Login" style="width:49%;"/>
                <input type="reset" name="reset" class="btn btn-info" value="Reset" style="width:50%;"/> 
                <input type="submit" name="register" class="btn btn-info" value="Register" style="width:100%; margin-top:5px;"/>
            </form>
        </div>
        <br />
    </body>
 </html>